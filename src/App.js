import React, { Component, Fragment } from "react";
import Explore from "./components/Explore";
import FullFund from "./components/FullFund";
import Home from "./components/Home";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <Router>
        <Fragment>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/explore/" component={Explore} />
            <Route exact path="/explore/:id" component={FullFund} />
          </Switch>
        </Fragment>
      </Router>
    );
  }
}

export default App;
