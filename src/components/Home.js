import React, { Component } from "react";
import Explore from "./Explore";
import { Link } from "react-router-dom";
export default class Home extends Component {
  render() {
    return (
      <div className="container mt-4">
        <Link to={"explore/"}>Funds Explore Page</Link>
      </div>
    );
  }
}
