import React, { Component, Fragment } from "react";
import axios from "axios";
import Fund from "./Fund";
class Explore extends Component {
  state = {
    funds: [],
    filterBy: "name",
  };
  componentDidMount() {
    axios.get("https://api.kuvera.in/api/v3/funds.json").then((response) => {
      const funds = response.data.slice(0, 100);
      const sortFunds = funds.sort((a, b) => {
        return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
      });
      this.setState({
        funds: sortFunds,
      });
    });
  }
  renderFunds = () => {
    const { funds } = this.state;
    return funds.map((fund) => {
      return <Fund fund={fund} key={fund.code} />;
    });
  };
  handleChange = (event) => {
    let filterBy = event.target.value;
    this.setState({
      [event.target.name]: event.target.value,
    });
    console.log(filterBy);
    axios.get("https://api.kuvera.in/api/v3/funds.json").then((response) => {
      const funds = response.data.slice(0, 100);
      const sortFunds = funds.sort((a, b) => {
        return a[filterBy] < b[filterBy]
          ? -1
          : a[filterBy] > b[filterBy]
          ? 1
          : 0;
      });
      this.setState({
        funds: sortFunds,
      });
    });
  };
  render() {
    const { filterBy } = this.state;
    return (
      <Fragment>
        <div className="container">
          <h4 className="text-center m-4 font-weight-bold">
            Funds Explore Page
          </h4>
          <table className="table">
            <thead>
              <tr>
                <th scope="col" className="from-control">
                  Name Of Fund
                </th>
                <th scope="col">Category</th>
                <th scope="col">Fund Type</th>
                <th scope="col">Plane</th>
                <th scope="col">Returns year_1</th>
                <th scope="col">Returns year_3</th>
                <th scope="col">Filter By</th>
                <th scope="col">
                  <select
                    name="filterBy"
                    className="form-control"
                    onChange={this.handleChange}
                    defaultValue={filterBy}
                  >
                    <option value="name">Name</option>
                    <option value="category">Fund Category</option>
                    <option value="fund_type">Fund Type</option>
                    <option value="plan">Plan</option>
                  </select>
                </th>
              </tr>
            </thead>
            <tbody>{this.renderFunds()}</tbody>
          </table>
        </div>
      </Fragment>
    );
  }
}

export default Explore;
