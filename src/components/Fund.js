import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";

export default class Fund extends Component {
  render() {
    const fund = this.props.fund;
    return (
      <Fragment>
        <tr scope="row">
          <td>
            <Link to={{ pathname: fund.code }}>{fund.name}</Link>
          </td>
          <td>{fund.category}</td>
          <td>{fund.fund_type}</td>
          <td>{fund.plan}</td>
          <td>{fund.returns.year_1}</td>
          <td>{fund.returns.year_3}</td>
        </tr>
      </Fragment>
    );
  }
}
